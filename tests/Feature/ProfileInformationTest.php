<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProfileInformationTest extends TestCase
{
    use RefreshDatabase;

    public function test_profile_information_can_be_updated(): void
    {
        $this->actingAs($user = User::factory()->create());

        $response = $this->put('/user/profile-information', [
            'firstname' => 'Test Firstname',
            'lastname' => 'Test Lastname',
            'email' => 'test@example.com',
        ]);

        $this->assertEquals('Test Firstname', $user->fresh()->firstname);
        $this->assertEquals('Test Lastname', $user->fresh()->lastname);
        $this->assertEquals('test@example.com', $user->fresh()->email);
    }
}
